# Canon Variations

## Descriptive Analysis of Philosophy Canons in English, French, and German

The original inspiration for this project was [Benjamin Schmidt's](https://benschmidt.org/) implementation of "bookworm" (Schmidt 2018) on top of the HathiTrust digital library (see https://bookworm.htrc.illinois.edu/develop/). Searching for "Descartes" in the three languages English, French, and German delivers this result:

![Screenshot bookworm search "Descartes" in three languages](<images/screenshot_bookworm.png>)

The graph shows the number of mentions of Descartes per a million words in the 7 million English (blue line), 1 million German (orange line) and 992000 French volumes of the HathiTrust library. And it demonstrates that the relevance of authors of the philosophical canon varies to a significant extent between philosophical traditions, insofar as these traditions are tied to vernacular languages. We may have to speak of "philosophical canons" in the plural instead of the philosophical canon in the singular. This project attempts to develop a first approximative argument for this thesis.

Descriptive and quantitative canon analysis is not yet a highly developed discipline. But we can distinguish different appraoches in the existing literature: you can either designate a genre as determining canonization directly. In this vein Demin / Kouprianov (2018) use the TOCs of 19th century German philosophy textbooks to determine the canonization of German philosophers, Kant, Herbart, and the German Idealists. Or you can use a broader range of sources and match them with a genre that serves as a proxy for canonization. Kampmann (2013) follows this strategy matching publisher catalogues with literary encyclopedias in order to describe processes of canonization in 20th century German literature.

This project will combine both strategies. We start from three collections ('worksets')  of English, German, and French books on the history of philosophy contained in HathiTrust. We will use the HathiTrust named entity recognition tool to determine for each of these three languages the 100 most prominent names of thinkers across the three subcorpora and identify those who appear in all three languages. We will then trace the frequency of mentions of these canonical thinkers in a broader corpus in the history of philosophy, using the Extracted Features Dataset from HathiTrust (Jett et al. 2020).

### The Structure of This Repository

* [step_1](step_1/) consists in the creation of the reference corpus for identifying canonical figures in the three linguistic traditions under consideration. The three lists of works to be included are [metadata_ger.csv](step_1/output/metadata_ger.csv), [metadata_fren.csv](step_1/output/metadata_fren.csv), and [metadata_eng.csv](step_1/output/metadata_eng.csv). The relevant code and comments can be found in a Jupyter notebook, [get_records_hist_phil.ipynb](step_1/get_records_hist_phil.ipynb).

* [step_2](step_2/) concerns the analysis of the reference corpus constituted in step 1: we employ Hathitrust's Named Entity Recognition algorithm in order to identify 
    * the 100 most named philosophers in each of the three subcorpora and 
    * those philosophers who appear as one of the 100 most named philosophers in all subcorpora. 
The [input](step_2/input) directory contains the raw data produced by the NER algorithm. [`parsing_NER.ipynb`](step_2/parsing_NER.ipynb) contains the code needed to clean the data and deliver an overview of the corpus effectively used for NER (not all volumes we identified in step 1 are actually available for this kind of analysis). [`reconcile_philosophers.ipynb`](step_2/reconcile_philosophers.ipynb) connects the names we identified in NER with unique identifiers in order to make lists of philosophers comparable between languages (Aristotle is 'Aristoteles' in German and 'Aristote' in French). [`philosophers_analysis.ipynb`](step_2/philosophers_analysis.ipynb) generates data about the ocurrence of the reconciled names of philosophers in the three subcorpora and some additional analysis. The list of philosophers to be traced in the search corpus can be found in [`step_2/output/phils_reconciled_with_lc.csv`](step_2/output/phils_reconciled_with_lc.csv).

* [step 3](step_3/) is dedicated to the creation of a search corpus derived from the list of canonical philosophers that was the main outcome of step 2. This step is guided by two methodological assumptions: 

	* We only look at works that have been assigned one of the canonical philosophers as a keyword. This means that we do not trace mentions of canonical philosophers in works that lack such an assignment (i. e. works on less prominent philosophers or works in 'systematic' philosophy).

	* We also look only at keyword assignments for philosophers that are prominent in all three philosophical traditions we are investigating, German, French, and English.

	* So in sum this means that we trace the history of the canon in that part of the historiography of philosophy that concerns itself with canonical thinkers.

We operationalise this conception by including those texts in Hathitrust for which the catalogue record contains the name of a philosopher from our list as a keyword. The exact form of the name is derived from the Library of Congress Authority File (LOCAF). In a few cases, this delivers no results, we then have used only the first word of the respective label ("Thales", "Seneca" etc.). We exclude duplicates and periodicals from our results. One historical figure is excluded, namely Jesus Christ. Since we we can anticipate that books with the keyword "Jesus Christ" will overwhelmingly be of a historical or theological rather than philosophical nature, we fear that the inclusion of such titles in the search corpus would distort our final results considerably.

We also excluded periodicals, because they are often multilingual publications that cannot be clearly grouped into one of our three traditions. Moreover, we are not sure about the coverage provided in this area by Hathitrust. Alternative data sources may be better suited to address this question. This, however, belongs into the rubric 'further work'. Further steps of data unification and cleaning are explained in the relevant notebook, [`clean_metadata_search.ipynb`](step_3/clean_metadata_search.ipynb). 

We have also conducted a preliminary analysis of the search corpus. looking at the distribution of our three subcorpora over time and finding clear predominance of German in the segment of philosophical research literature under consideration We also looked at the counts per tradition for canonical philosophers, again explained in detail in the respective notebok, [`analysis_search_corpus.ipynb`](step_3/analysis_search_corpus.ipynb).








### Licensing

All data and metadata created in this project are made available under the CC 0 Public Domain Dedication. If you want to reuse these data, please provide a citation to this repository, since this is still a work in progress. Final results will be made available through an appropriate repository, citation information will be provided.

Extracted features from HathiTrust are made available under a [CC 4.0 BY License](http://creativecommons.org/licenses/by/4.0/)


### References

Maxim Demin, Alexei Kouprianov (2018). *Studying Kanonbildung: An Exercise in a Distant Reading of Contemporary Self-descriptions of the 19th Century German Philosophy* in: *Social Epistemology* 32, 112-127.

Jacob Jett, Boris Capitanu, Deren Kudeki, Timothy Cole, Yuerong Hu, Peter Organisciak, Ted Underwood, Eleanor Dickson Koehl, Ryan Dubnicek, J. Stephen Downie (2020). *The HathiTrust Research Center Extracted Features Dataset (2.0).* HathiTrust Research Center. https://doi.org/10.13012/R2TE-C227 

Elisabeth Kampmann (2013). *Wie lässt sich ein Kanon rekonstruieren?* in: *Handbuch Kanon und Wertung: Theorien, Instanzen, Geschichte* (eds. Gabriele Ripple and Simone Winko), Stuttgart: Metzler, 407-412.

Benjamin Schmidt (2019). Hathitrust-Bookworm, URL: https://github.com/Bookworm-project/Hathitrust-Bookworm
