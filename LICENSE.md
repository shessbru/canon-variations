All original code and data in this repository is dedicated to the public domain ([CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/deed.en)).

Hathitrust extracted features are made available according to [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/).
