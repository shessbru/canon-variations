{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 194,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import re\n",
    "pd.options.mode.chained_assignment = None\n",
    "pd.options.display.max_rows = None\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Corpus preparation\n",
    "\n",
    "In order to limitate unintended distortions of the search corpus we take the following steps: \n",
    "\n",
    "* We excluded periodicals, because periodicals are often multilingual texts; this creates additional complexity and, concomitantly, the possibility of 'unforced errors'. \n",
    "\n",
    "* For multi-volume titles we unify the various abbreviations indicating the volume number, so that we can identify duplicates. We remove all titles with identical \"MARC 008 fields\" (physical description) and volume designation. This means that if there should exist two-volume works the volumes of which are exactly identical in their physical characteristics as they are captured in MARC 008 *and* are not designated as multi-volume titles in the 'enumcron' field of the original bibliographical record, we will keep only one of them. The risk of such omissions seems to be comparably small.\n",
    "\n",
    "* The unification of publication years is explained in detail below. Generally speaking we follow the maxim to use the earlier date, if two dates are given. We also do not use the explicit field for the publication year in the MARC record, data which are again quite messy, but are content to use the simplified machine-readable version of the date in field 008 of the MARC record.\n",
    "\n",
    "* We also found through manual inspection that the machine readable version of the language of publication is more reliable than the original field we had derived from parsing the data of the web catalogue. We therefore use only this field to determine the linguistic properties of a title.\n",
    "\n",
    "The structure of this notebook:\n",
    "\n",
    "* [Monographs vs. other forms of publication](#Monographs-vs.-other-forms-of-publication)\n",
    "* [Deduplication](#Deduplication)\n",
    "* [Unifying dates of publication](#Unifying-dates-of-publication)\n",
    "* [Language](#Language)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 33,
   "metadata": {},
   "outputs": [],
   "source": [
    "json_results_ger = pd.read_csv(\"output/json_meta_ger_raw.csv\")\n",
    "json_results_fren = pd.read_csv(\"output/json_meta_fren_raw.csv\")\n",
    "json_results_eng = pd.read_csv(\"output/json_meta_eng_raw.csv\")\n",
    "\n",
    "re_year = re.compile(r\"([0-9]{4})|\\([0-9]{4}\\)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Monographs vs. other forms of publication\n",
    "\n",
    "The MARC field 7 in the \"leader\" of the bibliographic record contains the \"bibliographic level\" of the volume under consideration. We remove serials (\"s\")."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "per_ger = json_results_ger[json_results_ger[\"ldr\"] == \"s\"]\n",
    "noper_ger = json_results_ger[json_results_ger[\"ldr\"] != \"s\"]\n",
    "per_ger.to_csv (\"output/periodicals_german.csv\")\n",
    "per_fren = json_results_fren[json_results_fren[\"ldr\"] == \"s\"]\n",
    "noper_fren = json_results_fren[json_results_fren[\"ldr\"] != \"s\"]\n",
    "per_fren.to_csv (\"output/periodicals_french.csv\")\n",
    "per_eng = json_results_eng[json_results_eng[\"ldr\"] == \"s\"]\n",
    "noper_eng = json_results_eng[json_results_eng[\"ldr\"] != \"s\"]\n",
    "per_eng.to_csv (\"output/periodicals_english.csv\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Deduplication"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 49,
   "metadata": {},
   "outputs": [],
   "source": [
    "def clean_multivol(vols_df):\n",
    "    \"\"\"\n",
    "    Unify volume designations; drop duplicates.\n",
    "    \"\"\"\n",
    "    vols_df[\"enumcron_clean\"] = vols_df[\"enumcron\"].str.strip()\n",
    "    vols_df[\"enumcron_clean\"] = vols_df[\"enumcron_clean\"].str.replace(re_year, \"Year\", regex=True)\n",
    "    replace_strings = [\"Bd.\", \"bd.\", \"V\", \"v. \", \"Th.\", \"t.\", \"v.0\", \"T.\", \"vol. \", \"vol.\"]\n",
    "    for repl in replace_strings:\n",
    "        vols_df[\"enumcron_clean\"] = vols_df[\"enumcron_clean\"].str.replace(repl, \"v.\", regex=False)\n",
    "        vols_df[\"enumcron_clean\"] = vols_df[\"enumcron_clean\"].str.replace(\"..\", \".\", regex=False)\n",
    "        vols_df[\"enumcron_clean\"] = vols_df[\"enumcron_clean\"].fillna(value=vols_df[\"enumcron\"])\n",
    "        \n",
    "    return(vols_df)\n",
    "\n",
    "noper_clean_enum_ger = clean_multivol(noper_ger)\n",
    "noper_clean_enum_fren = clean_multivol(noper_fren)\n",
    "noper_clean_enum_eng = clean_multivol(noper_eng)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 88,
   "metadata": {},
   "outputs": [],
   "source": [
    "noper_dedup_ger = noper_clean_enum_ger.drop_duplicates(subset=[\"marc_008\", \"enumcron_clean\"])\n",
    "\n",
    "noper_dedup_fren = noper_clean_enum_fren.drop_duplicates(subset=[\"marc_008\", \"enumcron_clean\"])\n",
    "\n",
    "noper_dedup_eng = noper_clean_enum_eng.drop_duplicates(subset=[\"marc_008\", \"enumcron_clean\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Unifying dates of publication\n",
    "\n",
    "The MARC field 0008 registers, among other data, the year of publication and the status of this date (see https://www.loc.gov/marc/umb/um07to10.html#part9). We find the following indicators:\n",
    "\n",
    "* \"s\": single known or probable date,\n",
    "* \"m\": multiple dates,\n",
    "* \"r\": reprint date and original date in second position,\n",
    "* \"c\": detailed date,\n",
    "* \"\\\": not in standard, also \"no attempt to code\"?\n",
    "* \"q\": questionable date,\n",
    "* \"t\": publication date and copyright date,\n",
    "* \"|\": no attempt to code,\n",
    "* \"?\": not in standard, leads to no recognizable date.\n",
    "\n",
    "One of these classifiers, \"?\", is not followed by a discernible date. We must add the date manually. Three classifiers entail exactly one date and pose no further problems: \"s\", \"t\", and \"|\". Some classifiers entail either one or two dates: \"\\\\\" (mostly one), \"q\" (timespan), \"t\" (sometimes Copyright date - from e. g. republication) is later than original date, copyright date is the first date, original date the second date). The remaining classifiers entail two dates. Either multiple dates apply (\"m\"), or an apparent date must be corrected (\"c\") or the book is a reprint (\"r) or the apparent date of publication is questionable (q\"). \n",
    "\n",
    "If we have simply multiple dates (\"m\"), we settle on the earlier date, i. e. the first date (this choice is arbitrary!). In the case of reprint dates (\"r\") we take the first date, i. e. the date of publication for the reprint, since this is when the book actually appeared in the library. The same applies to \"detailed dates\" (\"c\"). Since we have no clear criterion for \"\\\\\" we settle there on the first date as well. Questionable dates (\"q\") should be calculated as the average of the first (beginning of the timespan) and second date (end of the timespan), but the data for the second date are very messy. So for reasons of simplicity we settle for the first date. For the \"c\" classifier (copyright) we also choose the first date which is usually later and more probably the date for which we can assume availability of the book in the library. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 206,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We take the first date, \n",
    "\n",
    "\n",
    "def clean_years(df):\n",
    "    \"\"\"\n",
    "    Parse MARC field 008 for clean dates of publication. \n",
    "    We discard 17 records with no discernible date of publication.\n",
    "    For all other records we chose the first date, if two are available.\n",
    "    \"\"\"\n",
    "    \n",
    "    discard_list = [\"\\\\\\\\\\\\\\\\\", \"uuuu\", \"190u\", \"187u\", \"19uu\",\n",
    "                   \"18uu\", \"195u\", \"????\", \"||||\", \"191u\", \"196u\",\n",
    "                   \"48\\\\\\\\\", \"199u\"]\n",
    "    years_list = [\"1968.\", \"1973.\", \"1975\", \"1995.\", \"1932\", \"1979.\",\n",
    "                 \"1919.\", \"1934.\", \"1928.\", \"1991.\", \"1969.\"]\n",
    "          \n",
    "    df_1 = df[(df[\"marc_008\"].str[13:17].isin(discard_list))]\n",
    "    df_concat_1 = df_1[df_1[\"pubyear\"].isin(years_list)]\n",
    "    \n",
    "    df_concat_1[\"pubyear\"] = df_concat_1[\"pubyear\"].fillna(0)\n",
    "    df_concat_1[\"pubyear_clean\"] = df_concat_1[\"pubyear\"].str.strip(\".\").astype(int)\n",
    "    df_discard_1 = df_1[~(df_1[\"pubyear\"].isin(years_list))]\n",
    "    print(f\"{len(df_discard_1)} records discarded, because no date of publication.\")\n",
    "    \n",
    "    df_concat_2 = df[~(df[\"marc_008\"].str[13:17].isin(discard_list))&\n",
    "                    ~(df[\"htid\"]).isin(df_concat_1[\"htid\"])]\n",
    "    df_concat_2[\"pubyear_clean\"] = df_concat_2[\"marc_008\"].str[13:17].astype(int)\n",
    "    result = pd.concat([df_concat_1, df_concat_2])\n",
    "    return(result)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 219,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "26 records discarded, because no date of publication.\n",
      "4 records discarded, because no date of publication.\n",
      "14 records discarded, because no date of publication.\n"
     ]
    }
   ],
   "source": [
    "cleaned_years_ger = clean_years(noper_dedup_ger)\n",
    "cleaned_years_fren = clean_years(noper_dedup_fren)\n",
    "cleaned_years_eng = clean_years(noper_dedup_eng)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Language"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 234,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cleaning language in German corpus:\n",
      "Original number of volumes:\n",
      "10099\n",
      "Number of volumes with correct language in field 008:\n",
      "9637\n",
      "Cleaning language in French corpus:\n",
      "Original number of volumes:\n",
      "5319\n",
      "Number of volumes with correct language in field 008:\n",
      "4694\n",
      "Cleaning language in English corpus:\n",
      "Original number of volumes:\n",
      "8729\n",
      "Number of volumes with correct language in field 008:\n",
      "8280\n"
     ]
    }
   ],
   "source": [
    "def clean_language(df, lang):\n",
    "    \"\"\"\n",
    "    Remove titles with wrong language marker in MARC field 008.\n",
    "    \"\"\"\n",
    "    result = df[df[\"marc_008\"].str[-5:-2] == lang]\n",
    "    print(\"Original number of volumes:\")\n",
    "    print(len(df))\n",
    "    print(\"Number of volumes with correct language in field 008:\")\n",
    "    print(len(result))\n",
    "    return(result)\n",
    "\n",
    "print(\"Cleaning language in German corpus:\")\n",
    "cleaned_language_ger = clean_language(cleaned_years_ger, \"ger\")\n",
    "print(\"Cleaning language in French corpus:\")\n",
    "cleaned_language_fren = clean_language(cleaned_years_fren, \"fre\")\n",
    "print(\"Cleaning language in English corpus:\")\n",
    "cleaned_language_eng = clean_language(cleaned_years_eng, \"eng\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 235,
   "metadata": {},
   "outputs": [],
   "source": [
    "cleaned_language_ger.to_csv(\"output/json_meta_clean_ger.csv\")\n",
    "cleaned_language_fren.to_csv(\"output/json_meta_clean_fren.csv\")\n",
    "cleaned_language_eng.to_csv(\"output/json_meta_clean_eng.csv\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
